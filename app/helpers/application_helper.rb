module ApplicationHelper
	def get_following()
		@follow = Follow.where(:follower_id => current_user.id)
	end

	def get_followers()
		@follow = Follow.where(:target_id => current_user.id)
	end
end

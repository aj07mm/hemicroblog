module PostsHelper
	def is_post_owner?(post)
		post.user_id == current_user.id
	end
end

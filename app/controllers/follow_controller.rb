class FollowController < ApplicationController
 
  before_filter :authenticate_user!
	
  def follow_user 

    target_user = User.find(params[:follow_target])

	  json = current_user.follow(target_user)
	
	  respond_to do |format|
      format.json {render :json => json}
    end		
  end
end

// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require_tree .
//= require angular

origin = window.location.origin


var app = angular.module("myApp", []);


app.controller("myCtrl", function($scope, $http) {

	$http({
	    url: origin + '/users.json',
	    method: 'GET'
	}).success(function(data, status, headers, config) {
	    $scope.users = data;
	}).error(function(data, status, headers, config) {
	    $scope.status = status;
	});

  $scope.filterFunction = function(element) {
    return element.name.match(/^Ma/) ? true : false;
  };

});
 
app.controller("userShowCtrl", function($scope,$http){

	$scope.follow = function(obj,$event){

		document.getElementById('notice').style.display = 'block';

		$http({
		    url: origin + '/follow/follow_user.json',
		    method: 'GET',
		    params: {
		    	follow_target : obj.target.id
		    }
		}).success(function(data, status, headers, config) {
			  obj.target.innerHTML = data.button;
		      $scope.notice = data.feedback;
		      $( "#notice" ).fadeOut(2000, function() {
		      	this.style.display = 'none';
			  });
		}).error(function(data, status, headers, config) {
		    $scope.status = status;
		});
	
	}

});
	
$(function(){ 
	$(document).foundation();	
});


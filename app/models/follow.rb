 class Follow < ActiveRecord::Base
       #has_many :users, :foreign_key => "follows_id"
       #has_many :users, :foreign_key => "target_id"

       belongs_to :follower, :class_name => "User"
       belongs_to :target, :class_name => "User"

        validates_uniqueness_of :follower_id, :scope => [:target_id]
 end

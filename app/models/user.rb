class User < ActiveRecord::Base
  has_many :posts
  belongs_to :follows

  has_many :how_follows, :class_name => 'Follows', :foreign_key => 'follower_id'
  has_many :i_am_following, :class_name => 'Follows', :foreign_key => 'target_id'

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  	def follow(target_user)

	  	response = Follow.where(:follower_id => self.id).where(:target_id => target_user.id)

	  	msg = {}

		if response.empty?
		 	@follow = Follow.new
		  	@follow.follower_id = self.id 
		  	@follow.target_id = target_user.id
		  	if @follow.save
				msg[:feedback] = 'Você está agora seguindo este usuário'
				msg[:button] = 'UnFollow'
			end
		else
			response[0].destroy
			msg[:feedback] = 'Você deixou de seguir este usuário'
			msg[:button] = 'Follow'
		end

		json = msg.to_json
	end

end

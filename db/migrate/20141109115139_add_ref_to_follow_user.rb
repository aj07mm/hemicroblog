class AddRefToFollowUser < ActiveRecord::Migration
  def change
  	execute <<-SQL
      ALTER TABLE follows
        ADD CONSTRAINT fk_follows_follower_id
        FOREIGN KEY (follower_id)
        REFERENCES users(id)
    SQL
    execute <<-SQL
      ALTER TABLE follows
        ADD CONSTRAINT fk_follows_target_id
        FOREIGN KEY (target_id)
        REFERENCES users(id)
    SQL
  end
end

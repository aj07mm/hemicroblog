class ChangeFollowTypes < ActiveRecord::Migration
 def change
  change_column :follows, :follower_id, :integer
  change_column :follows, :target_id, :integer
 end
end

require 'rails_helper'

#rails soh

feature "OUsuarioDevePoders", :type => :feature do
  include Warden::Test::Helpers

  let(:user) { FactoryGirl.create(:user) } #segundo eh nome da factory

  #pending "add some scenarios (or delete) #{__FILE__}"
  scenario 'Realizar o login' do
  
    visit 'users/sign_in'

    fill_in 'user[email]', with: user.email
    fill_in 'user[password]', with: user.password
    
    click_button 'Log in' #value do button

     expect(page).to have_content 'Logout (email@email.com) My Posts Users About HEmicroblog Notificações: Signed in successfully.'
  end


end

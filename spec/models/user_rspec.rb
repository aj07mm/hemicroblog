require 'rails_helper'

RSpec.describe User, :type => :model do
   include Warden::Test::Helpers

  let(:user) { FactoryGirl.create(:user) }
  let(:user2) { FactoryGirl.create(:user, email: 'jaba@starwars.com' ) }
  #let(:invalid_user) { FactoryGirl.create(:invalid_user) }

  before :each do		 
		login_as user, scope: :user
  end
  
  context 'method follow' do
      it 'passando a seguir' do
        expect(user.follow(user2)).to eq('{"feedback":"Você está agora seguindo este usuário","button":"UnFollow"}')
      end
      it 'deixando de seguir' do
        user.follow(user2) #passando a seguir
        expect(user.follow(user2)).to eq('{"feedback":"Você deixou de seguir este usuário","button":"Follow"}')
      end
   end

end

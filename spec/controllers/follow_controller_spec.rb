require 'rails_helper'

RSpec.describe FollowController, :type => :controller do
  
  describe "GET follow_user" do

  	let(:user) { FactoryGirl.create(:user) } 
    let(:user2) { FactoryGirl.create(:user, email: 'jaba@starwars.com' ) }
    let!(:follow) { FactoryGirl.create(:follow, follower_id: user.id,target_id: user2.id) }

    it "returns http redirect 302" do
      get :follow_user
      expect(response).to have_http_status(:redirect)
    end

	  it "user is logged" do	  
	    
      sign_in user

      get :follow_user, follow_target: user2, :format => :json

      expect(response).to have_http_status(:success)
    end

  end


    
end

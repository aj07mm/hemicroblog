require "rails_helper"

RSpec.describe ApplicationHelper do
  include ActionView::Helpers
  include Warden::Test::Helpers

  let(:user) { FactoryGirl.create(:user) } #segundo eh nome da factory
  let(:user2) { FactoryGirl.create(:user, email: 'jaba@starwars.com' ) }
 
  let!(:follow) { FactoryGirl.create(:follow, follower_id: user.id,target_id: user2.id) }


  before :each do    
    login_as user, scope: :user
  end

  describe "Testes ApplicationHelper" do
    context 'get_following: retorna Array de users que sigo' do
      it 'user seguindo user2 - retorna user2' do
        view.stub(:current_user) { user }
        expect(helper.get_following()).to eq(Follow.where(:follower_id => user.id))
      end
    end
    context 'get_followers: retorna Array de users que me seguem' do
      it 'user seguindo user2 - retorna user2' do
        view.stub(:current_user) { user2 }
        expect(helper.get_following()).to eq(Follow.where(:follower_id => user2.id))
      end
    end
  end
end
require 'rails_helper'


RSpec.describe FollowHelper, :type => :helper do
  include ActionView::Helpers
  include Warden::Test::Helpers

  let(:user) { FactoryGirl.create(:user) } #segundo eh nome da factory
  let(:user2) { FactoryGirl.create(:user, email: 'jaba@starwars.com' ) }
 
  let!(:follow) { FactoryGirl.create(:follow, follower_id: user.id,target_id: user2.id) }


  before :each do		 
		login_as user, scope: :user
  end

  context 'am_i_following: retorna Follow se nao estiver seguindo' do
    it 'user seguindo user2 ? - retorna UnFollow' do
      view.stub(:current_user) { user }
      expect(helper.am_i_following(user2)).to eq('UnFollow')
    end
    it 'user2 seguindo user ? - retorna Follow' do
      view.stub(:current_user) { user2 }
      expect(helper.am_i_following(user)).to eq('Follow')
    end
  end

  end
